## Important 

To use this program you must already have the ESRI package software already downloaded to your C:\ drive.  

## Included Files

- **IceCreamFinder.jar** which is an executable jar file to use the software
- **QuickStart9eHotlink.java** which is the source code for the project

## Usage Information

- The ***table of contents***, or toc, is to the left of the map. 
Each entry is called a 'legend' and represents a map 'layer' or 
'theme'.  If you click on a legend, that layer is called the 
active layer, or selected layer.  Its display (rendering) properties 
can be controlled using the Legend Editor, and the legends can be 
reordered using Layer Control.  Both Legend Editor and Layer Control 
are separate Help Topics.

- The ***Legend Editor*** is a menu item found under the File menu. 
Given that a layer is selected by clicking on its legend in the table of 
contents, clicking on Legend Editor will open a window giving you choices 
about how to display that layer.  For example you can control the color 
used to display the layer on the map, or whether to use multiple colors 

- ***Layer Control*** is a Menu on the menu bar.  If you have selected a 
layer by clicking on a legend in the toc (table of contents) to the left of 
the map, then the promote and demote tools will become usable.  Clicking on 
promote will raise the selected legend one position higher in the toc, and 
clicking on demote will lower that legend one position in the toc.

- There are multiple zoom tools to use. If you click on the ***Zoom In*** tool, and
then click on the map, you will see a part of the map in greater detail.
Conversely, the when clicking on the map with the Zoom Out tool, the detail
is greatly reduced. Finally, the Zoom to Active Layer Tool is used by clicking
on a layer in the table of contents, then the actual button will become active
and you can click on it to zoom enough to see all the features from the 
selected layer.

- There are a variety of tools to learn about local ice cream locations.
The first is the ***hotlink tool*** - click on this tool in the toolbar and
then click on a point on the map display.  This will popup a window with information pertaining to that point such as name, address, and
coordinates.  Next is the ***menu tool*** - click on this tool and click
a point to get a list of flavors and options to choose from at each
location.  Another is the ***rating tool*** - click this tool and click a
point to get the ratings, the type of business it is, a web URL, and 
a description about the place. Finally, the ***distance tool*** allows you 
to measure your distance from one of the ice cream locations.
NOTE:  All these tools can be disabled by clicking another tool or 
clicking the ***Arrow tool***.