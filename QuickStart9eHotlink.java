import javax.swing.*;
import java.io.IOException;
import java.awt.event.*;
import java.awt.datatransfer.*;
import java.io.*;
import java.awt.*;
import com.esri.mo2.ui.bean.*; // beans used: Map,Layer,Toc,TocAdapter,
import com.esri.mo2.ui.dlg.AboutBox;
// TocEvent,Legend(a legend is part of a toc)
import com.esri.mo2.ui.tb.ZoomPanToolBar;
import com.esri.mo2.ui.tb.SelectionToolBar;
import com.esri.mo2.ui.ren.LayerProperties;
import com.esri.mo2.cs.geom.Envelope;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import com.esri.mo2.data.feat.*; //ShapefileFolder, ShapefileWriter
import com.esri.mo2.map.dpy.FeatureLayer;
import com.esri.mo2.file.shp.*;
import com.esri.mo2.map.dpy.Layerset;
import com.esri.mo2.map.draw.*;
import java.util.ArrayList;

public class QuickStart9eHotlink extends JFrame 
{
	static Map map = new Map();
	static boolean fullMap = true;  // Map not zoomed
	static String gotSearchText = null; 
	
	static double myLat; 
	static double myLon; 
	static double endLat; 
	static double endLon;
	
	
	static String[][] iceCreamFlavors = 
		{
				{"Cold Stone Creamery","Banana","Butter Pecan","Cake Batter","Candy Cane","Caramel Latte","Cheesecake","Chocolate","Cinnamon Bun",
									"Coffee","Dark Chocolate Peppermint","Eggnog","French Vanilla","Mint","Peanut Butter","Pecan Praline","Pistachio",
									"Strawberry","Sweet Cream"},     
				{"CREAM San Diego","Banana Walnut Fudge","Birthday Cake","Blueberry Cheesecake","Butterscotch","Chocoholic","Chocolate Chip Cookie Dough",
									"Cookies and Cream","Cup of Joe","French Vanilla","Mint Chocolate Chip","Peanut Butter Twist","Pistachio Delight",
									"Rocky Road","Salted Caramel","Very Berry Strawberry"},
				{"Hammond's Gourmet Ice Cream","Banana Nut","Birthday Cake","Chocolate","Chocolate Fudge","Jamocha Almond Fudge","Macadamia Nut",
									"Mexican Chocolate","Mint Chocolate Chip","Mint Oreo","Peanut Butter Brownie","Peanut Butter Snickers",
									"Peanut Butter Reeses","Pineapple Coconut Cream","Pistachio Cream","Salted Caramel","Salted Caramel Espresso",
									"Strawberry","Strawberry Cheesecake","Tahitian Vanilla Honey Macadamia Nut","Vanilla Malt Toffee","Vegan Chocolate",
									"Vegan Chocolate Coconut Macadamia Nut","White Chocolate","White Chocolate Macadamia Nut","White Pineapple Sorbet"},
				{"Tocumbo Ice Cream","Chocolate","Corn","DIABLITOS","Guanabana","Mamey","MaNgOnEaDaS","Nanche","OREO Cookies and Cream","Rose","Strawberry","Vanilla"},
				{"Baskin Robbins","America's Birthday Cake","Baseball Nut","Black Walnut","Caramel Cappuccino Cheesecake","Caramel Turtle Truffle","Cherries Jubilee",
									"Chocolate","Chocolate Chip","Chocolate Chip Cookie Dough","Eggnog","German Chocolate Cake","Gold Medal Ribbon","Icing on the Cake",
									"Jamoca","Jamoca Almond Fudge","Made with SNICKERS","Nutty Coconut","Old Fashioned Butter Pecan","OREO Cookies and Cream",
									"Peanut Butter and Chocolate","Pink Bubblegum","Pistachio Almond","Pralines and Cream","Pumpkin Pie","Reese's Peanut Butter Cup",
									"Rocky Road","Strawberry Cheesecake","Vanilla","Very Berry Strawberry","Wild and Reckless Sherbet","Winter White Chocolate",
									"World Class Chocolate"},
				{"Moosie's Ice Cream","Banana Walnut","Burgundy Cherry","Chocolate Dipped Strawberry","Cookie Dough","Dutch Chocolate","English Toffee","Fresh Lemon Sorbet",
									"Lemon Custard","Mint Chip","Mocha Almond Fudge","Passion Fruit Sorbet","Peppermint Candy","Pink Grapefruit Sorbet","Real Mango",
									"Real Pistachio Nut","Real Vanilla","Rocky Road","Strawberry Sorbet","Taro"},
				{"Mariposa Ice Cream","Banana Walnut","Berry Sorbet","Black Cherry","Butter Pecan","Butterfinger","Cappuccino Almond Fudge","Chocolate Chip Cookie Dough",
									"Chocolate","Chocolate Chip","Chocolate Malt","Chocolate Peanut Butter","Coconut","Coconut Almond Fudge","Coffee","Cookies and Cream",
									"Cookies and Mint","Eggnog","Heath Butter Toffee","Lemon","Lemon Sherbet","Lime Sherbet","Maple Walnut","Mexican Chocolate","Mint Chip",
									"Nuez","Peach","Peanut Butter Fudge","Peppermint","Pistachio","Pumpkin","Rocky Road","Rum Raisin","Strawberry","Vanilla","Vegan Maple",
									"Watermelon Sorbet","White Chocolate Raspberry Ripple"},
				{"Salt & Straw","Almond Brittle with Salted Ganache","Apple Brandy and Pecan Pie","Avocado and Oaxacan Chocolate Fudge","Chocolate Chip Cookie Dough",
									"Chocolate Gooey Brownie","Cinnamon Coconut Eggnog","Double Fold Vanilla","Freckled Woodblock Chocolate","Gingerbread Cookie Dough",
									"Honey Lavender","James Coffee and Bourbon","Malted Chocolate Chip Cookie Dough","Peanut Butter Stout with Chocolate Chicharron",
									"Peppermint Bark Cocoa","Roasted Strawberry and Toasted White Chocolate","Roasted Strawberry Coconut","Salted Chocolate Chip Cookie Dough",
									"Sea Salt with Caramel Ribbons","Sugar Plum Fairy"},
				{"Freeze San Diego","Black Magic","Chamango","Circus Cookie","Creme Brulee","Horchata","Milk and Cereal","Nutella","Peppermint Mocha","Peter Pandan","Red Bean",
									"S'Mores","Stawberry Coconut","Thai Tea","The Mermaid"},
				{"Golden Spoon","Banana","Banana Strawberry","Cake Batter","Cappuccino","Caramel","Chocolate Caramel","Chocolate Malt","Chocolate Mint","Classic Coffee",
									"Cookies and Cream","Cupcake","Double Vanilla","Eggnog","Espresso","Just Chocolate","Milk Chocolate","NY Cheesecake",
									"Old Fashioned Vanilla","Peanut Butter","Peppermint","Pistachio","Pumpkin Pie","Strawberry","Vanilla Malt","Winter Mint"},
				{"Daily Scoop","Banana","Blackberry Twist","Blueberry Cheesecake","Blueberry Sorbet","Brownies & Cream","Butter Rum Pecan","Butterscotch Marble",
									"Cantaloupe Sorbet","Cappuccinno Crunch","Champagne Sorbet","Chocolate Chip","Chocolate Mexicano","Chocolate Peanut Butter","Coconut",
									"Coconut Almond Joy","Coffee Almond Fudge","Dark Cherry","Eggnog","German Chocolate","Ginger","Golden Pistachio","Kahlua Krunch",
									"Kahlua Krunch","Lemon Sorbet","Lime Sorbet","Macadamia Nut Toffee","Mango Sorbet","Passion Fruit Sorbet","Pear Sorbet","Pineapple Sherbet",
									"Raspberry Cheesecake","Red Mint Chip","Red Raspberry Sorbet","Strawberry","Strawberry Cheesecake","Sweet Cream and Peanut Butter",
									"Triple Chocolate"},
				{"CrunchTime Popcorn & Ice Cream","Amaretto","Banana","Birthday Cake","Black Berry","Blueberry","Butter Pecan","Caramel","Cherry","Chocolate","Cinnamon","Coconut",
									"Double Dark Chocolate","Mint Chocolate","Peanut Butter","Strawberry","Vanilla"},
				{"Haagen-Dazs","Banana Peanut Butter Chip","Belgian Chocolate","Bourbon Praline Pecan","Butter Pecan","Caramel Cone","Cherry Vanilla","Chocolate",
									"Chocolate Chip Cookie Dough","Chocolate Chocolate Chip","Chocolate Peanut Butter","Coffee","Cookies and Cream","Dulce de Leche",
									"Espresso Choloate Cookie Crumble","Green Tea","Honey Salted Caramel Almond","Java Chip","Midnight Cookies and Cream","Mint Chip",
									"Peanut Butter Salted Fudge","Peppermint Bark","Pistachio","Pralines and Cream","Rocky Road","Rum Rasin","Strawberry","Vanilla",
									"Vanilla Chocolate Chip"},
				{"Creamistry","Birthday Cake","Black Cherry","Captain Crunch","Caramel","Cheesecake","Chocolate","Cinnamon Toast Crunch","Cocoa Puffs","Coconut","Cookie Butter",
									"OREO Cookies and Cream","Cotton Candy","Espresso","French Toast Crunch","French Vanilla","Fresh Banana","Fruity Pebbles",
									"Madagascar Vanilla Bean","Matcha Green Tea","Milk Coffee","Mint","Nutella","Pure Roasted Pistachio","Reese's Peanut Butter",
									"Sea Salt Caramel","Strawberry","Strawberry Milk","Taro","Thai Tea","Tiramisu"},
				{"Somi Somi","Banana","Black Sesame","Chocolate","Matcha","Milk","Milk Tea","OREO Cookies and Cream","Strawberry","Ube"}
		};

	
	Legend legend;
	Legend legend2;
	Layer layer = new Layer();																																
	Layer layer2 = new Layer();
	Layer layer3 = null;
	

	static AcetateLayer acetLayer;
	
	static com.esri.mo2.map.dpy.Layer layer4;
	com.esri.mo2.map.dpy.Layer activeLayer;
	int activeLayerIndex;
	static JMenuBar mbar = new JMenuBar();
	JMenu file = new JMenu("File");
	JMenu theme = new JMenu("Theme");
	JMenu layercontrol = new JMenu("LayerControl");
	
	JMenu help = new JMenu("Help");
	JMenu helptopics = new JMenu("Help Topics");
	JMenuItem tocitem = new JMenuItem("Table of Contents",new ImageIcon(getClass().getResource("helptopic.png")));
	JMenuItem legenditem = new JMenuItem("Legend Editor",new ImageIcon(getClass().getResource("helptopic.png")));
	JMenuItem layercontrolitem = new JMenuItem("Layer Control",new ImageIcon(getClass().getResource("helptopic.png")));
	JMenuItem zoomingitem = new JMenuItem("Zooming",new ImageIcon(getClass().getResource("helptopic.png")));
	JMenuItem icecreamitem = new JMenuItem("Ice Cream Tools",new ImageIcon(getClass().getResource("helptopic.png")));
	JMenuItem contactitem = new JMenuItem("Contact us");
	JMenuItem aboutitem = new JMenuItem("About MOJO...");
	
	static HelpTool helpTool= new HelpTool(); 

	
	JMenuItem attribitem = new JMenuItem("open attribute table", new ImageIcon("tableview.gif"));
	JMenuItem createlayeritem  = new JMenuItem("create layer from selection", new ImageIcon("Icon0915b.jpg"));
	static JMenuItem promoteitem = new JMenuItem("promote selected layer", new ImageIcon("promote1.gif"));
	JMenuItem demoteitem = new JMenuItem("demote selected layer", new ImageIcon("demote1.gif"));
	JMenuItem printitem = new JMenuItem("print",new ImageIcon(getClass().getResource("print.gif")));
	JMenuItem addlyritem = new JMenuItem("add layer",new ImageIcon(getClass().getResource("addtheme.gif")));
	JMenuItem remlyritem = new JMenuItem("remove layer",new ImageIcon(getClass().getResource("delete.gif")));
	JMenuItem propsitem = new JMenuItem("Legend Editor",new ImageIcon(getClass().getResource("properties.gif")));
	Toc toc = new Toc();
	String s1 = "C:\\ESRI\\FinalProjectLayers\\California.shp";
	String s2 = "C:\\ESRI\\FinalProjectLayers\\IceCreamLocations.shp";
	String datapathname = "";
	String legendname = "";
	ZoomPanToolBar zptb = new ZoomPanToolBar();
	static SelectionToolBar stb = new SelectionToolBar();
	JToolBar jtb = new JToolBar();
	ComponentListener complistener;
	JLabel statusLabel = new JLabel("status bar    LOC");
	static JLabel milesLabel = new JLabel("   DIST:  0 mi    ");
	static JLabel kmLabel = new JLabel("  0 km    ");
	java.text.DecimalFormat df = new java.text.DecimalFormat("0.000");
	JPanel myjp = new JPanel();
	JPanel myjp2 = new JPanel();
	JButton prtjb = new JButton(new ImageIcon(getClass().getResource("print.gif")));//getClass().getResource("print.gif")));
	JButton addlyrjb = new JButton(new ImageIcon(getClass().getResource("addtheme.gif")));
	JButton ptrjb = new JButton(new ImageIcon(getClass().getResource("pointer.gif")));
	JButton hotjb = new JButton(new ImageIcon(getClass().getResource("hotlink.gif")));
	
// _____________________________________________________________________________________________
	
	static boolean flavorMenuEnabled = false; 
	static boolean hotLinkEnabled = false; 
	static boolean ratingEnabled = false; 

	JButton flavorButton = new JButton(new ImageIcon(getClass().getResource("flavorList.png")));	
	JButton flavorSearchButton = new JButton(new ImageIcon(getClass().getResource("searchTool.png")));
	JButton ratingButton = new JButton(new ImageIcon(getClass().getResource("ratingTool.png")));
	JButton distjb = new JButton(new ImageIcon(getClass().getResource("measure_1.gif")));

// _____________________________________________________________________________________________
	
	Arrow arrow = new Arrow();
	ActionListener lis;
	ActionListener layerlis;
	ActionListener layercontrollis;
	TocAdapter mytocadapter;
	Toolkit tk = Toolkit.getDefaultToolkit();
	Image bolt = tk.getImage("C:\\Users\\travi\\eclipse-workspace\\QuickStart9eHotlink\\bin\\hotlink_32x32-32.gif");  // 16x16 gif file
	java.awt.Cursor boltCursor = tk.createCustomCursor(bolt,new java.awt.Point(11,26),"bolt");
	
	Image cone = tk.getImage("C:\\Users\\travi\\eclipse-workspace\\QuickStart9eHotlink\\bin\\cone.png");
	Image rate = tk.getImage("C:\\Users\\travi\\eclipse-workspace\\QuickStart9eHotlink\\bin\\rate.png");
	java.awt.Cursor flavorCursor = tk.createCustomCursor(cone, new java.awt.Point(15,31),"cone");	
	java.awt.Cursor ratingCursor = tk.createCustomCursor(rate, new java.awt.Point(6,27), "rate");
	
	
	MyPickAdapter picklis = new MyPickAdapter();
	Identify hotlink = new Identify(); //the Identify class implements a PickListener,
	Identify flavorMenu = new Identify();
	Identify ratingMenu = new Identify();
	
	static String myLocation = null;
	class MyPickAdapter implements PickListener   //implements hotlink
	{  
	    public void beginPick(PickEvent pe){};  // this fires even when you click outside the states layer
	    public void endPick(PickEvent pe){};
	    public void foundData(PickEvent pe)
	    {
	    	//fires only when a layer feature is clicked
	    	FeatureLayer flayer2 = (FeatureLayer) pe.getLayer();
	    	com.esri.mo2.data.feat.Cursor c = pe.getCursor();
	    	Feature f = null;
	    	System.out.println("inside foundData");
	    	Fields fields = null;
	    	if (c != null)
	    		f = (Feature)c.next();
	    	fields = f.getFields();
	    	String sname = fields.getField(2).getName(); //gets col. name for state name
	    	myLocation = (String)f.getValue(2);
	    	try 
	    	{
	    		if(QuickStart9eHotlink.hotLinkEnabled == true) 
	    		{
	    			HotPick hotpick = new HotPick();//opens dialog window with image in it
				    hotpick.setVisible(true);
	    		}
	    		if(QuickStart9eHotlink.flavorMenuEnabled == true)
	    		{
	    			HotPick flavorpick = new HotPick();
	    			flavorpick.setVisible(true);
	    		}
	    		if(QuickStart9eHotlink.ratingEnabled == true)
	    		{
	    			HotPick ratingpick = new HotPick();
	    			ratingpick.setVisible(true);
	    		}
			    
	    	} 
	    	catch(Exception e){}
	    }
	};

	static Envelope env;
	public QuickStart9eHotlink() {
		super("Quick Start");
		this.setBounds(150,150,900,650);
		zptb.setMap(map);
		stb.setMap(map);
		setJMenuBar(mbar);
		ActionListener lisZoom = new ActionListener() 
		{
			public void actionPerformed(ActionEvent ae)
			{
				fullMap = false;
			}
		}; // can change a boolean here
		ActionListener lisFullExt = new ActionListener() 
		{
			public void actionPerformed(ActionEvent ae)
			{
				fullMap = true;
			}
		};
		
		// next line gets ahold of a reference to the zoomin button
		JButton zoomInButton = (JButton)zptb.getActionComponent("ZoomIn");
		JButton zoomFullExtentButton = (JButton)zptb.getActionComponent("ZoomToFullExtent");
		JButton zoomToSelectedLayerButton = (JButton)zptb.getActionComponent("ZoomToSelectedLayer");
		zoomInButton.addActionListener(lisZoom);
		zoomFullExtentButton.addActionListener(lisFullExt);
		zoomToSelectedLayerButton.addActionListener(lisZoom);
		
		
		
		complistener = new ComponentAdapter () 
		{
			public void componentResized(ComponentEvent ce) 
			{
				if(fullMap) 
				{
					map.setExtent(env);
					map.zoom(1.0);    //scale is scale factor in pixels
					map.redraw();
				}
			}
		};
		
		addComponentListener(complistener);
		lis = new ActionListener() 
		{
			public void actionPerformed(ActionEvent ae)
			{
				Object source = ae.getSource();
				if (source == prtjb || source instanceof JMenuItem ) 
				{
					com.esri.mo2.ui.bean.Print mapPrint = new com.esri.mo2.ui.bean.Print();
			        mapPrint.setMap(map);
			        mapPrint.doPrint();// prints the map
				}
				else if (source == ptrjb) 
				{
					map.setSelectedTool(arrow);
				}
				else if (source == hotjb) 
				{
					hotlink.setCursor(boltCursor);
					map.setSelectedTool(hotlink);
					flavorMenuEnabled = false;
					ratingEnabled = false;
					hotLinkEnabled = true; 
				}				
				else if(source == flavorButton) 
				{
					flavorMenu.setCursor(flavorCursor);
					map.setSelectedTool(flavorMenu);
					flavorMenuEnabled = true; 
					ratingEnabled = false;
					hotLinkEnabled = false; 
				}

				else if(source == ratingButton)
				{
					ratingMenu.setCursor(ratingCursor);
					map.setSelectedTool(ratingMenu);
					ratingEnabled = true;
					hotLinkEnabled = false; 
					flavorMenuEnabled = false; 
				}
				else if(source == distjb)
				{
					DistanceTool distanceTool = new DistanceTool();
					map.setSelectedTool(distanceTool);
				}
				else 
				{
					try {
						AddLyrDialog aldlg = new AddLyrDialog();
						aldlg.setMap(map);
						aldlg.setVisible(true);
					} catch(IOException e){}
				}
			}
		};
    
		layercontrollis = new ActionListener() 
		{
			public void actionPerformed(ActionEvent ae)
			{
				String source = ae.getActionCommand();
				System.out.println(activeLayerIndex+" active index");
				if (source == "promote selected layer")
					map.getLayerset().moveLayer(activeLayerIndex,++activeLayerIndex);
				else
					map.getLayerset().moveLayer(activeLayerIndex,--activeLayerIndex);
				enableDisableButtons();
				map.redraw();
			}
		};
    
		layerlis = new ActionListener() 
		{
			public void actionPerformed(ActionEvent ae)
			{
				Object source = ae.getSource();
				if (source instanceof JMenuItem) 
				{
					String arg = ae.getActionCommand();
					if(arg == "add layer") 
					{
						try 
						{
							AddLyrDialog aldlg = new AddLyrDialog();
							aldlg.setMap(map);
							aldlg.setVisible(true);
						} catch(IOException e){}
					}
					else if(arg == "remove layer") 
					{
						try 
						{
							com.esri.mo2.map.dpy.Layer dpylayer = legend.getLayer();
							map.getLayerset().removeLayer(dpylayer);
							map.redraw();
							remlyritem.setEnabled(false);
							propsitem.setEnabled(false);
							attribitem.setEnabled(false);
							promoteitem.setEnabled(false);
							demoteitem.setEnabled(false);
							stb.setSelectedLayer(null);
							zptb.setSelectedLayer(null);
							stb.setSelectedLayers(null);
						} catch(Exception e) {}
					}
					else if(arg == "Legend Editor") 
					{
						LayerProperties lp = new LayerProperties();
						lp.setLegend(legend);
						lp.setSelectedTabIndex(0);
						lp.setVisible(true);
					}
					else if (arg == "open attribute table") 
					{
						try 
						{
							layer4 = legend.getLayer();
							AttrTab attrtab = new AttrTab();
							attrtab.setVisible(true);
						} catch(IOException ioe){}
					}
					else if (arg=="create layer from selection") {
						BaseSimpleRenderer sbr = new BaseSimpleRenderer();
						SimplePolygonSymbol sps = new SimplePolygonSymbol();
						sps.setPaint(AoFillStyle.getPaint(AoFillStyle.SOLID_FILL,new java.awt.Color(255,255,0)));
						sps.setBoundary(true);
						layer4 = legend.getLayer();
						FeatureLayer flayer2 = (FeatureLayer)layer4;
						// select, e.g., Montana and then click the
						// create layer menuitem; next line verifies a selection was made
						System.out.println("has selected" + flayer2.hasSelection());
						//next line creates the 'set' of selections
						if (flayer2.hasSelection()) 
						{
							SelectionSet selectset = flayer2.getSelectionSet();
							// next line makes a new feature layer of the selections
							FeatureLayer selectedlayer = flayer2.createSelectionLayer(selectset);
							sbr.setLayer(selectedlayer);
							sbr.setSymbol(sps);
							selectedlayer.setRenderer(sbr);
							Layerset layerset = map.getLayerset();
							// next line places a new visible layer, e.g. Montana, on the map
							layerset.addLayer(selectedlayer);
							//selectedlayer.setVisible(true);
							if(stb.getSelectedLayers() != null)
								promoteitem.setEnabled(true);
							try 
							{
								legend2 = toc.findLegend(selectedlayer);
							} catch (Exception e) {}

							CreateShapeDialog csd = new CreateShapeDialog(selectedlayer);
							csd.setVisible(true);
							Flash flash = new Flash(legend2);
							flash.start();
							map.redraw(); // necessary to see color immediately
						}
					}
				}
			}
		};
		
		toc.setMap(map);
		mytocadapter = new TocAdapter() 
		{
			public void click(TocEvent e) 
			{
				legend = e.getLegend();
				activeLayer = legend.getLayer();
				stb.setSelectedLayer(activeLayer);
				zptb.setSelectedLayer(activeLayer); // get active layer index for promote and demote
				activeLayerIndex = map.getLayerset().indexOf(activeLayer); // layer indices are in order added, not toc order.
				com.esri.mo2.map.dpy.Layer[] layers = {activeLayer};
				hotlink.setSelectedLayers(layers);// replaces setToc from MOJ10
				flavorMenu.setSelectedLayers(layers);
				ratingMenu.setSelectedLayers(layers);
				remlyritem.setEnabled(true);
				propsitem.setEnabled(true);
				attribitem.setEnabled(true);
				enableDisableButtons();
			}
		};
		
		map.addMouseMotionListener(new MouseMotionAdapter() 
		{
			public void mouseMoved(MouseEvent me) 
			{
				com.esri.mo2.cs.geom.Point worldPoint = null;
				if (map.getLayerCount() > 0) {
					worldPoint = map.transformPixelToWorld(me.getX(),me.getY());
					String s = "X:"+df.format(worldPoint.getX())+" "+"Y:"+df.format(worldPoint.getY());
					statusLabel.setText(s);
				}
				else
					statusLabel.setText("X:0.000 Y:0.000");
			}
		});
    
		toc.addTocListener(mytocadapter);
		remlyritem.setEnabled(false); // assume no layer initially selected
		propsitem.setEnabled(false);
		attribitem.setEnabled(false);
		promoteitem.setEnabled(false);
		demoteitem.setEnabled(false);
		printitem.addActionListener(lis);
		addlyritem.addActionListener(layerlis);
		remlyritem.addActionListener(layerlis);
		propsitem.addActionListener(layerlis);
		attribitem.addActionListener(layerlis);
		createlayeritem.addActionListener(layerlis);
		promoteitem.addActionListener(layercontrollis);
		demoteitem.addActionListener(layercontrollis);
		file.add(addlyritem);
		file.add(printitem);
		file.add(remlyritem);
		file.add(propsitem);
		theme.add(attribitem);
    	theme.add(createlayeritem);
    	layercontrol.add(promoteitem);
    	layercontrol.add(demoteitem);
    	mbar.add(file);
    	mbar.add(theme);
    	mbar.add(layercontrol);
	    prtjb.addActionListener(lis);
	    prtjb.setToolTipText("print map");
	    addlyrjb.addActionListener(lis);
	    addlyrjb.setToolTipText("add layer");
	    hotlink.addPickListener(picklis);
	    hotlink.setPickWidth(15);// sets tolerance for hotlink clicks
	    hotjb.addActionListener(lis);
	    hotjb.setToolTipText("hotlink tool - click a point to see pictures and info");
	    ptrjb.addActionListener(lis);
	    ptrjb.setToolTipText("pointer");    
	    jtb.add(prtjb);
	    jtb.add(addlyrjb);
	    jtb.add(ptrjb);
	    jtb.add(hotjb);
	   
		
	    
	    flavorMenu.addPickListener(picklis);
	    flavorMenu.setPickWidth(15);
	    ratingMenu.addPickListener(picklis);
	    ratingMenu.setPickWidth(15);
	    flavorButton.addActionListener(lis); 
	    flavorButton.setToolTipText("menu tool - click a point to see ice cream flavors there");
	    jtb.add(flavorButton);
	    ratingButton.addActionListener(lis); 
	    ratingButton.setToolTipText("rating tool - click a point to see that location's ratings and info");
		jtb.add(ratingButton);
	    jtb.add(flavorSearchButton);
	    flavorSearchButton.setToolTipText("flavor search tool - enter a flavor you want to find");	    
	    distjb.addActionListener(lis);
	    
	    
	    jtb.add(distjb);
	    distjb.setToolTipText("distance tool - check the distance between two clicked points");
	     
	    
	    
	    
//____________________________________________________________________
	    ActionListener helplis;
	      
	    helplis = new ActionListener()
        {
	    	public void actionPerformed(ActionEvent ae)
	    	{
	    		Object source = ae.getSource();
	    		if (source instanceof JMenuItem) 
	    		{
	    			String arg = ae.getActionCommand();
	    			if(arg == "About MOJO...") 
	    			{
	    				AboutBox aboutbox = new AboutBox();
	    				aboutbox.setProductName("MOJO");
	    				aboutbox.setProductVersion("2.0");
	    				aboutbox.setVisible(true);
	    				aboutbox.setLocation(100,100);
	    			}
	    			else if(arg == "Contact us") 
	    			{
	    				try 
	    				{
	    					String s = "\n\n\n\n                Any enquiries should be addressed to " +
	    							"\n\n\n                        travisdattilo@yahoo.com";
	    					HelpDialog helpdialog = new HelpDialog(s);
	    					helpdialog.setVisible(true);
	    				} catch(IOException e){}
	    			}
	    			else if(arg == "Table of Contents") 
	    			{
	    				try 
	    				{
	    					HelpDialog helpdialog = new HelpDialog((String)helpText.get(0));
	    					helpdialog.setVisible(true);
	    				} catch(IOException e){}
	    			}
	    			else if(arg == "Legend Editor") 
	    			{
	    				try 
	    				{
	    					HelpDialog helpdialog = new HelpDialog((String)helpText.get(1));
	    					helpdialog.setVisible(true);
	    				} catch(IOException e){}
	    			}
	    			else if(arg == "Layer Control") 
	    			{
	    				try 
	    				{
	    					HelpDialog helpdialog = new HelpDialog((String)helpText.get(2));
	    					helpdialog.setVisible(true);
	    				} catch(IOException e){}
	    			}
	    			else if(arg == "Zooming")
	    			{
	    				try 
	    				{
	    					HelpDialog helpdialog = new HelpDialog((String)helpText.get(3));
	    					helpdialog.setVisible(true);
	    				} catch(IOException e){}
	    			}
	    			else if(arg == "Ice Cream Tools")
	    			{
	    				try 
	    				{
	    					HelpDialog helpdialog = new HelpDialog((String)helpText.get(4));
	    					helpdialog.setVisible(true);
	    				} catch(IOException e){}
	    			}
	    		}
	    	}};
	    
	    
	    

	    
	    tocitem.addActionListener(helplis);
	    legenditem.addActionListener(helplis);
	    layercontrolitem.addActionListener(helplis);
	    zoomingitem.addActionListener(helplis);
	    icecreamitem.addActionListener(helplis);
	    
	    contactitem.addActionListener(helplis);
	    aboutitem.addActionListener(helplis);
    
	    help.add(helptopics);
	    helptopics.add(tocitem);
	    helptopics.add(legenditem);
	    helptopics.add(layercontrolitem);
	    helptopics.add(zoomingitem);
	    helptopics.add(icecreamitem);
	    help.add(contactitem);
	    help.add(aboutitem);
	    mbar.add(help);
	    
	    flavorButton.addActionListener(new ActionListener() 
	    {
	    	public void actionPerformed(ActionEvent e) 
	    	{
	    		if(flavorButton.isEnabled())
	    		{
					QuickStart9eHotlink.hotLinkEnabled = false; 
					QuickStart9eHotlink.flavorMenuEnabled = true; 
	    		}
	    	}
	    });
	    
	    hotjb.addActionListener(new ActionListener() 
	    {
	    	public void actionPerformed(ActionEvent e) 
	    	{
	    		if(hotjb.isEnabled())
	    		{
					QuickStart9eHotlink.hotLinkEnabled = true; 
					QuickStart9eHotlink.flavorMenuEnabled = false; 
	    		}
	    	}
	    });
	    
	    JDialog searchDialog = new JDialog(new JFrame(),"Search for an ice cream flavor");
	    JDialog outputDialog = new JDialog(new JFrame(),"Locations for the specified flavor: ");
	    JTextField textField = new JTextField(35);
	    JPanel textPanel = new JPanel(); 
	    JPanel outputPanel = new JPanel(); 
	    JLabel outputLabel = new JLabel();
	    JLabel whatToDoLabel = new JLabel("Enter an ice-cream flavor name to find locations with that flavor:");
	    
	    searchDialog.setSize(600, 150);
	    outputDialog.setSize(400,400);
	    outputPanel.setSize(400,400);
	    searchDialog.setLayout(new FlowLayout());
	    searchDialog.add(whatToDoLabel);
		searchDialog.add(textPanel,BorderLayout.NORTH);
		outputDialog.add(outputPanel,BorderLayout.NORTH);
		
		
		
		//outputPanel.setBounds(5,310,450,100);	// set color to tan
		outputPanel.setBackground(new Color(238,223,204));//255,250,240));
		
		
		
		JScrollPane scrollPane = new JScrollPane(outputPanel,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		outputDialog.add(scrollPane);
		
	
	    flavorSearchButton.addActionListener(new ActionListener() 
	    {
	    	public void actionPerformed(ActionEvent e) 
	    	{
	    		if(flavorSearchButton.isEnabled())
	    		{
					searchDialog.setVisible(true);
	    		}
	    	}
	    });
	       
	    textField.addActionListener(new ActionListener()
		{
	    	public void actionPerformed(ActionEvent e)
	    	{
	    		if(textField.getText() != null)
	    		{
	    			gotSearchText = textField.getText(); 
	    			//outputLabel.setText(gotSearchText);
	    			String newText = ""; 
	    			boolean gotLocation; 
	    			int count = 0; 
	    			for(int i = 0; i < iceCreamFlavors.length; i++)	// loop over all subsets
	    			{
	    				gotLocation = false;
	    				for(int j = 0; j < iceCreamFlavors[i].length; j++) // loop through a subset
	    				{
	    					if((iceCreamFlavors[i][j].toLowerCase()).contains(gotSearchText.toLowerCase()))
	    					{
	    						if(gotLocation == false)
    							{
	    							if(j != 0) // so that it does not display the Location name as a flavor
	    							{
	    								newText += "<HTML><BR><BR>" + iceCreamFlavors[i][0] + " has: <BR>&nbsp;&nbsp;\u2022 " + iceCreamFlavors[i][j];
	    							}
	    							gotLocation = true; 
    							}
	    						else
	    						{
	    							newText += "<BR>&nbsp;&nbsp;\u2022 " + iceCreamFlavors[i][j];	    							
	    						}
	    						
	    					}
	    				}
	    			}
	    			outputLabel.setText(newText);
	    			outputDialog.setVisible(true);
	    		}
	    	}
		});
	    
	    
		textPanel.add(textField);
		outputPanel.add(outputLabel);
	    

	    //____________________________________________________________________
	    myjp.add(jtb);
	    myjp.add(zptb);
	    myjp.add(stb);
	    myjp2.add(statusLabel);
	    myjp2.add(milesLabel);
	    myjp2.add(kmLabel);
	    getContentPane().add(map, BorderLayout.CENTER);
	    getContentPane().add(myjp,BorderLayout.NORTH);
	    getContentPane().add(myjp2,BorderLayout.SOUTH);
	    addShapefileToMap(layer,s1);
	    addShapefileToMap(layer2,s2);
	    getContentPane().add(toc, BorderLayout.WEST);
	    
	    
	    setuphelpText(); 
	    
	    
	    
	    // Make layer show up with ice cream symbol and always appear baby blue _______________________________vvvvvvvv
	    
	    java.util.List list = toc.getAllLegends();
	    
	    com.esri.mo2.map.dpy.Layer lay0 = ((Legend)list.get(0)).getLayer();  // get layer to change the symbol 
	    com.esri.mo2.map.dpy.Layer lay1 = ((Legend)list.get(1)).getLayer();  	// 

	    
	    FeatureLayer flayer0 = (FeatureLayer)lay0;
	    FeatureLayer flayer1 = (FeatureLayer)lay1;
	    
	    BaseSimpleRenderer bsr0 = (BaseSimpleRenderer)flayer0.getRenderer();
	    BaseSimpleRenderer bsr1 = (BaseSimpleRenderer)flayer1.getRenderer();
	    
	    SimplePolygonSymbol sim1 = (SimplePolygonSymbol)bsr1.getSymbol(); 
	    sim1.setPaint(AoFillStyle.getPaint(AoFillStyle.SOLID_FILL,new Color(150,200,255)));	// set color 
	    
	    com.esri.mo2.map.draw.RasterMarkerSymbol rms = new com.esri.mo2.map.draw.RasterMarkerSymbol();
			//rms.setSizeX(24); //size in pixels
			//rms.setSizeY(24);
		rms.setImageString("C:/esri orig/moj20/examples/icons/icecream.png");	// symbol path
	    bsr0.setSymbol(rms);	// set the symbol to the icecream cone
	    bsr1.setSymbol(sim1);
		
	    // _________________________________________________________________________________________always baby blue and ice cream icons ^^^
    
	}
	private void addShapefileToMap(Layer layer,String s) 
	{
		String datapath = s; //"C:\\ESRI\\MOJ10\\Samples\\Data\\USA\\States.shp";
		layer.setDataset("0;"+datapath);
		map.add(layer);
	}
	
	private void setuphelpText()
    {
		String s0 =
    		      "     The table of contents, or toc, is to the left of the map. \n" +
    		      "     Each entry is called a 'legend' and represents a map 'layer' or \n" +
    		      "     'theme'.  If you click on a legend, that layer is called the \n" +
    		      "     active layer, or selected layer.  Its display (rendering) properties \n" +
    		      "     can be controlled using the Legend Editor, and the legends can be \n" +
    		      "     reordered using Layer Control.  Both Legend Editor and Layer Control \n" +
    		      "     are separate Help Topics.";
	    helpText.add(s0);
	    String s1 = "    The Legend Editor is a menu item found under the File menu. \n" +
	    			"    Given that a layer is selected by clicking on its legend in the table of \n" +
	    			"    contents, clicking on Legend Editor will open a window giving you choices \n" +
	    			"    about how to display that layer.  For example you can control the color \n" +
	    			"    used to display the layer on the map, or whether to use multiple colors ";
	    helpText.add(s1);
	    String s2 = "    Layer Control is a Menu on the menu bar.  If you have selected a \n"+
	    			"    layer by clicking on a legend in the toc (table of contents) to the left of \n" +
	    			"    the map, then the promote and demote tools will become usable.  Clicking on \n" +
	    			"    promote will raise the selected legend one position higher in the toc, and \n" +
	    			"    clicking on demote will lower that legend one position in the toc.";
	    helpText.add(s2);
	    String s3 = "    There are multiple zoom tools to use. If you click on the Zoom In tool, and\n" +
	    			"    then click on the map, you will see a part of the map in greater detail.\n" +
	    		    "    Conversely, the when clicking on the map with the Zoom Out tool, the detail\n "+
	    			"    is greatly reduced. Finally, the Zoom to Active Layer Tool is used by clicking\n" + 
	    		    "    on a layer in the table of contents, then the actual button will become active\n" + 
	    			"    and you can click on it to zoom enough to see all the features from the \n"+ 
	    		    "    selected layer.";
	    helpText.add(s3);
	    String s4 = "    There are a variety of tools to learn about local ice cream locations.\n " + 
	    			"    The first is the hotlink tool - click on this tool in the toolbar and\n" + 
	    			"    then click on a point on the map display.  This will popup a window \n" + 
	    			"    with information pertaining to that point such as name, address, and\n" + 
	    			"    coordinates.  Next is the menu tool - click on this tool and click\n" + 
	    			"    a point to get a list of flavors and options to choose from at each\n" + 
	    			"    location.  Another is the rating tool - click this tool and click a\n" + 
	    			"    point to get the ratings, the type of business it is, a web URL, and \n" + 
	    			"    a description about the place. Finally, the distance tool allows you\n" + 
	    			"    to measure your distance from one of the ice cream locations.\n\n"+
	    			"    NOTE:  All these tools can be disabled by clicking another tool or \n" + 
	    			"                 clicking the Arrow tool.";
	    helpText.add(s4);
    }
	
	public static void main(String[] args) 
	{
		QuickStart9eHotlink qstart = new QuickStart9eHotlink();
		qstart.addWindowListener(new WindowAdapter() 
		{
			public void windowClosing(WindowEvent e) 
			{
				System.out.println("Thanks, Quick Start exits");
				System.exit(0);
			}
		});
		qstart.setVisible(true);
		env = map.getExtent();
	}
	
	private void enableDisableButtons() 
	{
		int layerCount = map.getLayerset().getSize();
		if (layerCount < 2) {
			promoteitem.setEnabled(false);
			demoteitem.setEnabled(false);
		}
		else if (activeLayerIndex == 0) {
			demoteitem.setEnabled(false);
			promoteitem.setEnabled(true);
		}
		else if (activeLayerIndex == layerCount - 1) {
			promoteitem.setEnabled(false);
			demoteitem.setEnabled(true);
		}
		else {
			promoteitem.setEnabled(true);
			demoteitem.setEnabled(true);
		}
	}
	private ArrayList helpText = new ArrayList(3);
}

class HelpDialog extends JDialog {
	  JTextArea helptextarea;
	  public HelpDialog(String inputText) throws IOException {
	    setBounds(70,70,460,250);
	      setTitle("Help");
	      addWindowListener(new WindowAdapter() {
	        public void windowClosing(WindowEvent e) {
	          setVisible(false);
	        }
	    });
	      helptextarea = new JTextArea(inputText,7,40);
	      JScrollPane scrollpane = new JScrollPane(helptextarea);
	    helptextarea.setEditable(false);
	    getContentPane().add(scrollpane,"Center");
	  }
	}
class HelpTool extends Tool {
}
// following is an Add Layer dialog window
class AddLyrDialog extends JDialog {
	Map map;
	ActionListener lis;
	JButton ok = new JButton("OK");
	JButton cancel = new JButton("Cancel");
	JPanel panel1 = new JPanel();
	com.esri.mo2.ui.bean.CustomDatasetEditor cus = new com.esri.mo2.ui.bean.
    CustomDatasetEditor();
	AddLyrDialog() throws IOException 
	{
		setBounds(50,50,520,430);
		setTitle("Select a theme/layer");
		addWindowListener(new WindowAdapter() 
		{
			public void windowClosing(WindowEvent e) 
			{
				setVisible(false);
			}
		});
	    lis = new ActionListener() {
	    	public void actionPerformed(ActionEvent ae) 
	    	{
	    		Object source = ae.getSource();
	    		if (source == cancel)
	    			setVisible(false);
	    		else 
	    		{
	    			try 
	    			{
	    				setVisible(false);
	    				map.getLayerset().addLayer(cus.getLayer());
	    				map.redraw();
	    				if (QuickStart9eHotlink.stb.getSelectedLayers() != null)
	    				{
	    					QuickStart9eHotlink.promoteitem.setEnabled(true);
	    				}
	    			} catch(IOException e){}
	    		}
	    	}
	    };
    
	   	ok.addActionListener(lis);
	    cancel.addActionListener(lis);
	    getContentPane().add(cus,BorderLayout.CENTER);
	    panel1.add(ok);
	    panel1.add(cancel);
	    getContentPane().add(panel1,BorderLayout.SOUTH);
  }
  
	public void setMap(com.esri.mo2.ui.bean.Map map1)
	{
		map = map1;
	}
}

class AttrTab extends JDialog 
{
	JPanel panel1 = new JPanel();
	com.esri.mo2.map.dpy.Layer layer = QuickStart9eHotlink.layer4;
	JTable jtable = new JTable(new MyTableModel());
	JScrollPane scroll = new JScrollPane(jtable);
	public AttrTab() throws IOException 
	{
		setBounds(70,70,450,350);
		setTitle("Attribute Table");
		addWindowListener(new WindowAdapter() 
		{
			public void windowClosing(WindowEvent e) 
			{
				setVisible(false);
			}
		});
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		// next line necessary for horiz scrollbar to work
		jtable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		TableColumn tc = null;
		int numCols = jtable.getColumnCount();
    	//jtable.setPreferredScrollableViewportSize(
		//new java.awt.Dimension(440,340));
		for (int j=0;j<numCols;j++) 
		{
			tc = jtable.getColumnModel().getColumn(j);
			tc.setMinWidth(50);
		}
		getContentPane().add(scroll,BorderLayout.CENTER);
	}
}

class MyTableModel extends AbstractTableModel 
{
	// the required methods to implement are getRowCount,
	// getColumnCount, getValueAt
	com.esri.mo2.map.dpy.Layer layer = QuickStart9eHotlink.layer4;
	MyTableModel() 
	{
		qfilter.setSubFields(fields);
		com.esri.mo2.data.feat.Cursor cursor = flayer.search(qfilter);
		while (cursor.hasMore()) 
		{
			ArrayList inner = new ArrayList();
			Feature f = (com.esri.mo2.data.feat.Feature)cursor.next();
			inner.add(0,String.valueOf(row));
			for (int j=1;j<fields.getNumFields();j++) 
			{
				inner.add(f.getValue(j).toString());
			}
			data.add(inner);
			row++;
		}
	}
	FeatureLayer flayer = (FeatureLayer) layer;
	FeatureClass fclass = flayer.getFeatureClass();
	String columnNames [] = fclass.getFields().getNames();
	ArrayList data = new ArrayList();
	int row = 0;
	int col = 0;
	BaseQueryFilter qfilter = new BaseQueryFilter();
	Fields fields = fclass.getFields();
	public int getColumnCount() 
	{
		return fclass.getFields().getNumFields();
	}
	public int getRowCount() 
	{
		return data.size();
	}
	public String getColumnName(int colIndx) 
	{
		return columnNames[colIndx];
	}
	public Object getValueAt(int row, int col) 
	{
		ArrayList temp = new ArrayList();
		temp =(ArrayList) data.get(row);
		return temp.get(col);
	}
}

class CreateShapeDialog extends JDialog 
{
	String name = "";
	String path = "";
	JButton ok = new JButton("OK");
	JButton cancel = new JButton("Cancel");
	JTextField nameField = new JTextField("enter layer name here, then hit ENTER",25);
	com.esri.mo2.map.dpy.FeatureLayer selectedlayer;
	ActionListener lis = new ActionListener() {
		public void actionPerformed(ActionEvent ae) 
		{
			Object o = ae.getSource();
			if (o == nameField) 
			{
				name = nameField.getText().trim();
				path = ((ShapefileFolder)(QuickStart9eHotlink.layer4.getLayerSource())).getPath();
				System.out.println(path+"    " + name);
			}
			else if (o == cancel)
				setVisible(false);
			else 
			{
				try 
				{
					ShapefileWriter.writeFeatureLayer(selectedlayer,path,name,2);
				} catch(Exception e) {System.out.println("write error");}
				setVisible(false);
			}
		}
	};

	JPanel panel1 = new JPanel();
	JLabel centerlabel = new JLabel();
	//centerlabel;
	CreateShapeDialog (com.esri.mo2.map.dpy.FeatureLayer layer5) {
		selectedlayer = layer5;
		setBounds(40,350,450,150);
		setTitle("Create new shapefile?");
		addWindowListener(new WindowAdapter() 
		{
			public void windowClosing(WindowEvent e) 
			{
				setVisible(false);
			}
		});
		nameField.addActionListener(lis);
		ok.addActionListener(lis);
		cancel.addActionListener(lis);
		String s = "<HTML> To make a new shapefile from the new layer, enter<BR>" + 
				"the new name you want for the layer and click OK.<BR>" +
				"You can then add it to the map in the usual way.<BR>"+
				"Click ENTER after replacing the text with your layer name";
		centerlabel.setHorizontalAlignment(JLabel.CENTER);
		centerlabel.setText(s);
		getContentPane().add(centerlabel,BorderLayout.CENTER);
		panel1.add(nameField);
		panel1.add(ok);
		panel1.add(cancel);
		getContentPane().add(panel1,BorderLayout.SOUTH);
	}
}

class Arrow extends Tool 
{
	public void mouseClicked(MouseEvent me){}
    
}

class Flash extends Thread 
{
	Legend legend;
	Flash(Legend legendin) 
	{
		legend = legendin;
	}
	public void run() 
	{
		for (int i=0;i<12;i++) {
			try {
				Thread.sleep(500);
				legend.toggleSelected();
			} catch (Exception e) {}
		}
	}
}

class HotPick extends JDialog 
{
	String myLocation = QuickStart9eHotlink.myLocation;	// my ice cream location
	String address = null;
	String coordinates = null;
	String phoneNum = null; 
	String picture = null;
	
	String locationName = "";
	String eachFlavor = "";
	String eachScoop = "";
	String eachMilkshake = "";
	String eachSundae = "";
	String eachTopping = "";
	
	String busType = "";
	String whereType = "";
	String website = "";
	String description = "";
	
	
	
	JPanel displayPanel = new JPanel();
	JPanel jpanel2 = new JPanel();	
	
	JLabel infoLabel = new JLabel();
	
	JLabel menuTitleLabel = new JLabel(); 
	JLabel menuFlavorsLabel = new JLabel();
	JLabel menuScoopsLabel = new JLabel();
	JLabel menuMilkshakeLabel = new JLabel();
	JLabel menuSundaeLabel = new JLabel();
	JLabel menuToppingsLabel = new JLabel();
	
	JPanel menuTitlePanel = new JPanel();
	JPanel menuFlavorsPanel = new JPanel();
	JPanel menuScoopsPanel = new JPanel();
	JPanel menuToppingsPanel = new JPanel(); 
	JPanel menuMilkshakePanel = new JPanel();
	JPanel menuSundaePanel = new JPanel();
	
	String[][] iceCreamLocations =
	{ 
			// Name, 							Address, 												geographic coordinates, 	phone number, 		picture
			{"Cold Stone Creamery",				"6145 El Cajon Blvd H, San Diego, CA 92115",			"32.76084 N, 117.06587 W",	"(619)583-3361",	"coldstone.PNG"},
			{"CREAM San Diego",					"5157 College Avenue Suite B, San Diego, CA 92115",		"32.77229 N, 117.06960 W",	"(619)230-5177",	"cream.PNG"},
			{"Hammond's Gourmet Ice Cream",		"3077 University Ave, San Diego, CA 92104",				"32.74824 N, 117.12752 W",	"(619)220-0231",	"hammond.PNG"},
			{"Tocumbo Ice Cream",				"4686 Market St, San Diego, CA 92102",					"32.71181 N, 117.09389 W",	"(619)264-0391",	"tocumbo.PNG"},
			{"Baskin Robbins",					"8807 1/2 La Mesa Blvd, La Mesa, CA 91941",				"32.76835 N, 117.01541 W",	"(619)462-7215",	"BR.PNG"},
			{"Moosie's Ice Cream",				"4073 Adams Ave, San Diego, CA 92116",					"32.76298 N, 117.10769 W",	"(619)450-6470",	"moosie.PNG"},
			{"Mariposa Ice Cream",				"3450 Adams Ave, San Diego, CA 92116",					"32.76359 N, 117.11897 W",	"(619)284-5197",	"mariposa.PNG"},
			{"Salt & Straw",					"1670 India St, San Diego, CA 92101",					"32.72268 N, 117.16847 W",	"(619)542-9394",	"saltstraw.PNG"},
			{"Freeze San Diego",				"3904 Convoy St #119, San Diego, CA 92111",				"32.81486 N, 117.15462 W",	"(858)987-0405",	"freeze.PNG"},
			{"Golden Spoon",					"5640 Lake Murray Blvd, La Mesa, CA 91942",				"32.78208 N, 117.03146 W",	"(619)337-7666",	"goldenspoon.PNG"},
			{"Daily Scoop",						"3004 Juniper St, San Diego, CA 92104",					"32.72983 N, 117.12933 W",	"(619)624-0920",	"dailyscoop.PNG"},
			{"CrunchTime Popcorn & Ice Cream",	"611 K St suite c, San Diego, CA 92101",				"32.70832 N, 117.16000 W",	"(619)338-0048",	"crunchtime.PNG"},
			{"Haagen-Dazs",						"7007 Friars Rd, San Diego, CA 92108",					"32.76824 N, 117.16686 W",	"(619)297-0135",	"haagendazs.PNG"},
			{"Creamistry",						"7420 Clairemont Mesa Blvd #108, San Diego, CA 92111",	"32.83336 N, 117.15834 W",	"(858)874-6407",	"creamistry.PNG"},
			{"Somi Somi",						"4620 Convoy St, San Diego, CA 92111",					"32.82426 N, 117.15572 W",	"(858)939-0388",	"somisomi.PNG"}
	};
	
	String[][] iceCreamFlavors = QuickStart9eHotlink.iceCreamFlavors; 
	
	String[][] iceCreamMenu = 
	{
		// name								//scoops 																										          							// milkshakes	//sundaes 				//Toppings
		{"Cold Stone Creamery",				"<HTML>&nbsp;&nbsp;\u2022 Like It (3.5oz)<BR>&nbsp;&nbsp;\u2022 Love It (5oz)<BR>&nbsp;&nbsp;\u2022 Gotta Have It (8oz)", 							"Yes",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 Almond Joy<BR>&nbsp;&nbsp;\u2022 Butterfinger<BR>&nbsp;&nbsp;\u2022 Chocolate Chips<BR>&nbsp;&nbsp;\u2022 Chocolate Shavings<BR>&nbsp;&nbsp;\u2022 Caramel Squares<BR>&nbsp;&nbsp;\u2022 Gumballs<BR>&nbsp;&nbsp;\u2022 Gummy Bears<BR>&nbsp;&nbsp;\u2022 Heath Bar<BR>&nbsp;&nbsp;\u2022 Kit Kat<BR>&nbsp;&nbsp;\u2022 M&M's<BR>&nbsp;&nbsp;\u2022 Reese's Peanut Butter Cup<BR>&nbsp;&nbsp;\u2022 Snickers<BR>&nbsp;&nbsp;\u2022 Twix<BR>&nbsp;&nbsp;\u2022 Chocolate Chip Cookies<BR>&nbsp;&nbsp;\u2022 Rainbow Sprinkles<BR>&nbsp;&nbsp;\u2022 Chocolate Sprinkles<BR>&nbsp;&nbsp;\u2022 OREO Cookies<BR>&nbsp;&nbsp;\u2022 Caramel<BR>&nbsp;&nbsp;\u2022 Cinnamon<BR>&nbsp;&nbsp;\u2022 Fudge<BR>&nbsp;&nbsp;\u2022 Honey<BR>&nbsp;&nbsp;\u2022 Nutella"},
		{"CREAM San Diego",					"<HTML>&nbsp;&nbsp;\u2022 Single<BR>&nbsp;&nbsp;\u2022 Double<BR>&nbsp;&nbsp;\u2022 Triple",														"Yes",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 Almonds<BR>&nbsp;&nbsp;\u2022 Caramel Sauce<BR>&nbsp;&nbsp;\u2022 Cherries<BR>&nbsp;&nbsp;\u2022 Cookies<BR>&nbsp;&nbsp;\u2022 Chocolate Sauce<BR>&nbsp;&nbsp;\u2022 Fruity Pebbles<BR>&nbsp;&nbsp;\u2022 Gummy Bears<BR>&nbsp;&nbsp;\u2022 Heath Toffee Bar<BR>&nbsp;&nbsp;\u2022 Hot Fudge<BR>&nbsp;&nbsp;\u2022 Mini Chocolate Chips<BR>&nbsp;&nbsp;\u2022 Mini Marshmallows<BR>&nbsp;&nbsp;\u2022 Mini M&M's<BR>&nbsp;&nbsp;\u2022 Nutella<BR>&nbsp;&nbsp;\u2022 OREO Cookies<BR>&nbsp;&nbsp;\u2022 Peanuts<BR>&nbsp;&nbsp;\u2022 Rainbow Sprinkles<BR>&nbsp;&nbsp;\u2022 Whipped Cream"},
		{"Hammond's Gourmet Ice Cream",		"<HTML>&nbsp;&nbsp;\u2022 Small<BR>&nbsp;&nbsp;\u2022 Medium<BR>&nbsp;&nbsp;\u2022 Large", 															"Yes",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 OREO Cookies<BR>&nbsp;&nbsp;\u2022 Rainbow Sprinkles<BR>&nbsp;&nbsp;\u2022 Gummy Bears<BR>&nbsp;&nbsp;\u2022 Hot Fudge<BR>&nbsp;&nbsp;\u2022 Caramel<BR>&nbsp;&nbsp;\u2022 Marshmallow"},
		{"Tocumbo Ice Cream",				"<HTML>&nbsp;&nbsp;\u2022 1 Scoop<BR>&nbsp;&nbsp;\u2022 2 Scoop<BR>&nbsp;&nbsp;\u2022 3 Scoop<BR>&nbsp;&nbsp;\u2022 8 Scoop",						"No",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 None"},
		{"Baskin Robbins",					"<HTML>&nbsp;&nbsp;\u2022 Kids Scoop<BR>&nbsp;&nbsp;\u2022 Single Scoop<BR>&nbsp;&nbsp;\u2022 Double Scoop<BR>&nbsp;&nbsp;\u2022 Triple Scoop",		"Yes",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 Rainbow Sprinkles<BR>&nbsp;&nbsp;\u2022 Chocolate Sprinkles<BR>&nbsp;&nbsp;\u2022 M&M's<BR>&nbsp;&nbsp;\u2022 OREO Cookies<BR>&nbsp;&nbsp;\u2022 Gummy Bears<BR>&nbsp;&nbsp;\u2022 Almonds<BR>&nbsp;&nbsp;\u2022 Strawberry<BR>&nbsp;&nbsp;\u2022 Pineapple<BR>&nbsp;&nbsp;\u2022 Marshmallow Sauce<BR>&nbsp;&nbsp;\u2022 Hot Fudge<BR>&nbsp;&nbsp;\u2022 Caramel<BR>&nbsp;&nbsp;\u2022 Whipped Cream"},
		{"Moosie's Ice Cream",				"<HTML>&nbsp;&nbsp;\u2022 Cora Cone<BR>&nbsp;&nbsp;\u2022 Single Scoop<BR>&nbsp;&nbsp;\u2022 Double Scoop<BR>&nbsp;&nbsp;\u2022 Three Scoop", 		"Yes",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 Brownie<BR>&nbsp;&nbsp;\u2022 Hot Fudge<BR>&nbsp;&nbsp;\u2022 Caramel<BR>&nbsp;&nbsp;\u2022 Coffee<BR>&nbsp;&nbsp;\u2022 Nuts<BR>&nbsp;&nbsp;\u2022 Chocolate Sauce"},
		{"Mariposa Ice Cream",				"<HTML>&nbsp;&nbsp;\u2022 Single Scoop<BR>&nbsp;&nbsp;\u2022 Double Scoop",																			"Yes",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 Walnuts<BR>&nbsp;&nbsp;\u2022 Hot Fudge<BR>&nbsp;&nbsp;\u2022 Caramel<BR>&nbsp;&nbsp;\u2022 Whipped Cream<BR>&nbsp;&nbsp;\u2022 Chocolate Sprinkles<BR>&nbsp;&nbsp;\u2022 Caramel<BR>&nbsp;&nbsp;\u2022 Bananas"},
		{"Salt & Straw",					"<HTML>&nbsp;&nbsp;\u2022 Kids Scoop<BR>&nbsp;&nbsp;\u2022 Single Scoop<BR>&nbsp;&nbsp;\u2022 Double Scoop<BR>&nbsp;&nbsp;\u2022 Split Scoop",		"Yes",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 Nuts<BR>&nbsp;&nbsp;\u2022 Hot Fudge<BR>&nbsp;&nbsp;\u2022 Caramel<BR>&nbsp;&nbsp;\u2022 Whipped Cream<BR>&nbsp;&nbsp;\u2022 Rainbow Sprinkles<BR>&nbsp;&nbsp;\u2022 Chocolate Sprinkles<BR>&nbsp;&nbsp;\u2022 Chocolate Sauce<BR>&nbsp;&nbsp;\u2022 Gummy Bears"},
		{"Freeze San Diego",				"<HTML>&nbsp;&nbsp;\u2022 Single<BR>&nbsp;&nbsp;\u2022 Double",																						"No",			"No",					"<HTML>&nbsp;&nbsp;\u2022 None"},																		
		{"Golden Spoon",					"<HTML>&nbsp;&nbsp;\u2022 Small<BR>&nbsp;&nbsp;\u2022 Regular",																						"No",			"No",					"<HTML>&nbsp;&nbsp;\u2022 None"},
		{"Daily Scoop",						"<HTML>&nbsp;&nbsp;\u2022 Child<BR>&nbsp;&nbsp;\u2022 Single,<BR>&nbsp;&nbsp;\u2022 Double",														"Yes",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 Hot Fudge<BR>&nbsp;&nbsp;\u2022 Caramel<BR>&nbsp;&nbsp;\u2022 Marshmallow<BR>&nbsp;&nbsp;\u2022 White Chocolate<BR>&nbsp;&nbsp;\u2022 Peanut Butter<BR>&nbsp;&nbsp;\u2022 Whipped Cream<BR>&nbsp;&nbsp;\u2022 Nuts<BR>&nbsp;&nbsp;\u2022 Cherry"},
		{"CrunchTime Popcorn & Ice Cream",	"<HTML>&nbsp;&nbsp;\u2022 Child<BR>&nbsp;&nbsp;\u2022 Regular<BR>&nbsp;&nbsp;\u2022 Large",															"No",			"No",					"<HTML>&nbsp;&nbsp;\u2022 None"},
		{"Haagen-Dazs",						"<HTML>&nbsp;&nbsp;\u2022 1 Scoop<BR>&nbsp;&nbsp;\u2022 2 Scoops,<BR>&nbsp;&nbsp;\u2022 3 Scoops<BR>&nbsp;&nbsp;\u2022 4 Scoops",					"No",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 Caramel<BR>&nbsp;&nbsp;\u2022 Stawberry<BR>&nbsp;&nbsp;\u2022 Chocolate<BR>&nbsp;&nbsp;\u2022 Rasberry<BR>&nbsp;&nbsp;\u2022 Whipped Cream<BR>&nbsp;&nbsp;\u2022 Chocolate Sprinkles<BR>&nbsp;&nbsp;\u2022 Chocolate Pearls<BR>&nbsp;&nbsp;\u2022 Multi-Color Sprinkles<BR>&nbsp;&nbsp;\u2022 Nutes<BR>&nbsp;&nbsp;\u2022 Almond Flakes"},
		{"Creamistry",						"<HTML>&nbsp;&nbsp;\u2022 Single<BR>&nbsp;&nbsp;\u2022 Double",																						"Yes",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 Chocolate Curls<BR>&nbsp;&nbsp;\u2022 Heath<BR>&nbsp;&nbsp;\u2022 Kit Kat<BR>&nbsp;&nbsp;\u2022 Mini M&M's<BR>&nbsp;&nbsp;\u2022 Mini Chocolate Chips<BR>&nbsp;&nbsp;\u2022 Mini Gummy Bears<BR>&nbsp;&nbsp;\u2022 Mini Marshmallows<BR>&nbsp;&nbsp;\u2022 Mochi<BR>&nbsp;&nbsp;\u2022 Rainbow Sprinkles<BR>&nbsp;&nbsp;\u2022 Reese's Cups<BR>&nbsp;&nbsp;\u2022 Twix<BR>&nbsp;&nbsp;\u2022 Brownie Bites<BR>&nbsp;&nbsp;\u2022 Cap'n Crunch<BR>&nbsp;&nbsp;\u2022 Cheesecake Bites<BR>&nbsp;&nbsp;\u2022 Cinnamon Toast Crunch<BR>&nbsp;&nbsp;\u2022 Cocoa Puffs<BR>&nbsp;&nbsp;\u2022 Cookie Dough<BR>&nbsp;&nbsp;\u2022 French Toast Crunch<BR>&nbsp;&nbsp;\u2022 Fruity Pebbles<BR>&nbsp;&nbsp;\u2022 OREO Cookies<BR>&nbsp;&nbsp;\u2022 Banana<BR>&nbsp;&nbsp;\u2022 Strawberry<BR>&nbsp;&nbsp;\u2022 Nuts<BR>&nbsp;&nbsp;\u2022 Blueberry Sauce<BR>&nbsp;&nbsp;\u2022 Caramel<BR>&nbsp;&nbsp;\u2022 Chololate Fudge<BR>&nbsp;&nbsp;\u2022 Honey<BR>&nbsp;&nbsp;\u2022 Kiwi<BR>&nbsp;&nbsp;\u2022 Mango<BR>&nbsp;&nbsp;\u2022 Marshmallow Cream<BR>&nbsp;&nbsp;\u2022 Nutella<BR>&nbsp;&nbsp;\u2022 Whipped Cream"},		
		{"Somi Somi",						"<HTML>&nbsp;&nbsp;\u2022 Swirl",																													"No",			"Yes",					"<HTML>&nbsp;&nbsp;\u2022 Nutella<BR>&nbsp;&nbsp;\u2022 Cheddar<BR>&nbsp;&nbsp;\u2022 Custard<BR>&nbsp;&nbsp;\u2022 Red Bean<BR>&nbsp;&nbsp;\u2022 Taro"}
	};
	
	String[][] iceCreamRatings = 
	{
		// Name, 							rating picture,		Corp or Mom & Pop	Where				Website													description	
		{"Cold Stone Creamery",				"threestars.png",	"Corporation",      "Country Wide",		"https://www.coldstonecreamery.com",					"An ice cream chain that allows the customer to design their own <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + 
																																								"ice cream creations with hand-mixed ice cream.",},
		{"CREAM San Diego",					"fourstars.png",	"Corporation",		"San Diego Only",	"https://creamnation.com/location/san-diego",			"On the counter service for ice cream sandwiches with cookies, <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + 
																																								"scoops, milkshakes, and more."},
		{"Hammond's Gourmet Ice Cream",		"fivestars.png",	"Mom and Pop",		"San Diego Only",	"https://www.hammondsgourmet.com",						"Offers premium, handmade, and rich ice cream.  Offering over <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + 
																																								"300 flavors you'll want to try them all."},
		{"Tocumbo Ice Cream",				"threestars.png",	"Mom and Pop",		"San Diego Only",	"http://www.tocumboicecream.com/",						"With Mexican style ice cream treats, Tocumbo is a great place <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + 
																																								"to get lunch and enjoy some delicious dessert."},
		{"Baskin Robbins",					"fivestars.png",	"Corporation",		"Country Wide",		"http://www.baskinrobbins.com/",						"American chain of ice cream and ice cream cakes that was founded <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + 
																																								"in 1945 and has be known for its premium dessert options"},
		{"Moosie's Ice Cream",				"fivestars.png",	"Mom and Pop",		"San Diego Only",	"https://moosiesicecream.wordpress.com/",				"Ice cream shop that offers a large variety of premium, handmade, <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + 
																																								"traditional, special, and seasonal ice cream flavors"},
		{"Mariposa Ice Cream",				"fivestars.png",	"Mom and Pop",		"San Diego Only",	"http://www.mariposaicecream.com/",						"In the heart of Normal Heights, San Diego, Mariposa Ice Cream takes <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
																																								"pride in their homemade ice cream.  They have run their business for <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + 
																																								"16 years and welcome new people to try their delicious flavors."},
		{"Salt & Straw",					"fourstars.png",	"Mom an Pop",		"San Diego Only",	"https://saltandstraw.com/", 							"With the love of ice cream, family, and local foods, the founder of <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"Salt & Straw, Kim, made an ice cream shop in 1996 and created a place<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"where you can enjoy a delicious treat while running into your neighbors,<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"celebrating with your family, or just rewarding yourself."},
		{"Freeze San Diego",				"fourstars.png",	"Mom and Pop",		"San Diego Only",	"http://freezesd.com/",									"A warm welcome in the store, but a freezing cold treat to enjoy. Freeze San<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"Diego offers made-to-order liquid nitrogen ice cream that offers a chill down<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"your spin and delight to your taste buds."},
		{"Golden Spoon",					"fourstars.png",	"Corporation",		"San Diego Only",	"https://twitter.com/goldenspoon?lang=en",				"A gourmet frozen yogurt and ice cream company dedicated to selling delicious <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"creamy delights."},
		{"Daily Scoop",						"fourstars.png",	"Mom and Pop",		"San Diego Only",	"https://www.yelp.com/biz/the-daily-scoop-san-diego",	"All natural homemade ice cream just like the old fashioned way since the late '40s."},
		{"CrunchTime Popcorn & Ice Cream",	"fourstars.png",	"Mom and Pop",		"San Diego Only",	"https://www.crunchtimepopcorn.com/",					"CrunchTime started from a loving couple who had a love for ice cream.  They draw <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"on the hot summer days of their childhoods and enjoying ice cream, popcorn, and <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"snacks while hanging out with their friends."},
		{"Haagen-Dazs",						"threestars.png",	"Corporation",		"Country Wide",		"https://www.haagendazs.us/",							"Haagen-dazs is a well established company from 1961 that aims to offer a huge <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"selection of ice cream choices."},
		{"Creamistry",						"fourstars.png",	"Corporation",		"Country Wide",		"https://creamistry.com/",								"Creamistry offers hand-crafted liquid nitrogen premium ice cream, one scoop at a<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"time.  They take pride in their high quality and premium ingredients to create <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"delicious sweet ice cream treats."},
		{"Somi Somi",						"fivestars.png",	"Mom and Pop",		"San Diego Only",	"https://www.somisomi.com/",							"Soft Serve ice cream dessert served in a special fish shaped cone, Somi Somi is an<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
																																								"enjoyable environment to get an enjoyable dessert. "}
	};
	
	
	HotPick() throws IOException 
	{
		setTitle("This was your pick");
		setBounds(250,250,700,500);
		addWindowListener(new WindowAdapter() 
		{
			public void windowClosing(WindowEvent e) 
			{
				setVisible(false);
			}
		});
		if(QuickStart9eHotlink.hotLinkEnabled == true) 
		{
			for (int i = 0;i<20;i++)  
			{
				if (iceCreamLocations[i][0].equals(myLocation)) 
				{
					address = iceCreamLocations[i][1];
					coordinates = iceCreamLocations[i][2];
					phoneNum = iceCreamLocations[i][3];
					picture = iceCreamLocations[i][4];
					infoLabel.setText(	// &nbsp; is non breaking space in html to nicely line up information in dialog window
							"<HTML>Business Name:&nbsp;&nbsp;&nbsp;&nbsp;" + myLocation +
							"<BR>Address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + address +
							"<BR>Coordinates:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + coordinates +
							"<BR>Phone Number:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + phoneNum);
					break;	
				}	
			}
			ImageIcon iceCreamPicture = new ImageIcon(getClass().getResource(picture));
			JLabel picLabel = new JLabel(iceCreamPicture);
			displayPanel.add(infoLabel); 
			jpanel2.add(picLabel);
			displayPanel.setVisible(true);
			infoLabel.setVisible(true);
			setVisible(true);
			getContentPane().add(jpanel2,BorderLayout.CENTER);
			getContentPane().add(displayPanel,BorderLayout.SOUTH);
		}
		if(QuickStart9eHotlink.flavorMenuEnabled == true) 
		{
			
			for (int i = 0;i<20;i++)  
			{
				if (iceCreamMenu[i][0].equals(myLocation)) 
				{
					for(int j = 1; j < iceCreamMenu[i].length; j++) 
					{
						eachScoop = iceCreamMenu[i][1];
						eachMilkshake = iceCreamMenu[i][2];
						eachSundae = iceCreamMenu[i][3];
						eachTopping = iceCreamMenu[i][4];
					}
					menuScoopsLabel.setText("<HTML>Scoop Options: <BR> " + eachScoop);
					menuMilkshakeLabel.setText("<HTML>Milkshake Availability: " + eachMilkshake + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sundae Availability: " + eachSundae);
					//menuSundaeLabel.setText("<HTML>Sundae Availability: " + eachSundae);
					menuToppingsLabel.setText("<HTML>Topping Options: <BR> " + eachTopping);
					
				}	
				if (iceCreamFlavors[i][0].equals(myLocation)) 
				{
					for(int j = 1; j < iceCreamFlavors[i].length; j++) 
					{
						locationName = iceCreamFlavors[i][0] + " - Menu<BR>";
						eachFlavor += "&nbsp;&nbsp;\u2022 " + iceCreamFlavors[i][j] + " <BR>";	
					}
					menuTitleLabel.setText("<HTML><U><font size =+2>" + locationName + "</font size =+2></U>");
					menuFlavorsLabel.setText("<HTML>" + menuFlavorsLabel.getText() + eachFlavor);
					break;
				}	
			}
			menuTitlePanel.add(menuTitleLabel); 
			menuFlavorsPanel.add(menuFlavorsLabel);
			menuScoopsPanel.add(menuScoopsLabel);
			menuToppingsPanel.add(menuToppingsLabel);
			menuMilkshakePanel.add(menuMilkshakeLabel);
			//menuSundaePanel.add(menuSundaeLabel);
			
			menuTitleLabel.setVisible(true);
			menuFlavorsLabel.setVisible(true);
			menuScoopsLabel.setVisible(true);
			menuToppingsLabel.setVisible(true);
			menuMilkshakeLabel.setVisible(true);
			//menuSundaePanel.setVisible(true);
			
			
			menuTitlePanel.setVisible(true);
			menuFlavorsPanel.setVisible(true);
			menuScoopsPanel.setVisible(true);
			menuToppingsPanel.setVisible(true);
			menuMilkshakePanel.setVisible(true);
			menuTitlePanel.setBackground(new Color(255,240,245));	
			menuFlavorsPanel.setBackground(new Color(255,240,245));
			menuScoopsPanel.setBackground(new Color(255,240,245));
			menuToppingsPanel.setBackground(new Color(255,240,245));
			menuToppingsPanel.setBackground(new Color(255,240,245));
			menuMilkshakePanel.setBackground(new Color(255,240,245));
			
			
			//menuSundaePanel.setVisible(true);
			setLocationRelativeTo(QuickStart9eHotlink.mbar);
			setVisible(true);
			getContentPane().setLayout(new BorderLayout());
			getContentPane().add(menuTitlePanel,BorderLayout.PAGE_START);
			getContentPane().add(menuFlavorsPanel,BorderLayout.LINE_START);
			getContentPane().add(menuScoopsPanel,BorderLayout.LINE_END);
			getContentPane().add(menuToppingsPanel,BorderLayout.CENTER);
			getContentPane().add(menuMilkshakePanel,BorderLayout.PAGE_END);
			//getContentPane().add(menuSundaePanel,BorderLayout.PAGE_END);
			setSize(700,710);
			
		}
		if(QuickStart9eHotlink.ratingEnabled == true) 
		{
			for (int i = 0;i<20;i++)  
			{
				if (iceCreamRatings[i][0].equals(myLocation)) 
				{
					
					picture = iceCreamRatings[i][1];
					busType = iceCreamRatings[i][2];
					whereType = iceCreamRatings[i][3];
					website = iceCreamRatings[i][4];
					description = iceCreamRatings[i][5];
					infoLabel.setText("<HTML>Business Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + myLocation + 
							            "<BR>Type of Business:&nbsp;&nbsp;&nbsp;" + busType + 
							            "<BR>Where:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + whereType + 
							            "<BR>Website URL:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + website + 
							            "<BR>Description:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + description);
					break;	
				}	
			}
			ImageIcon ratingPicture = new ImageIcon(getClass().getResource(picture));
			JLabel picLabel = new JLabel(ratingPicture);
			displayPanel.add(infoLabel); 
			jpanel2.add(picLabel);
			displayPanel.setBackground(new Color(229,255,229));
			jpanel2.setBackground(new Color(229,255,229));
			
			infoLabel.setVisible(true);
			setVisible(true);
			setSize(750,350);
			getContentPane().add(jpanel2,BorderLayout.NORTH);
			getContentPane().add(displayPanel,BorderLayout.CENTER);
		}
		
	}
}


class DistanceTool extends DragTool  {
	  int startx,starty,endx,endy,currx,curry;
	  boolean dragging;
	  com.esri.mo2.cs.geom.Point initPoint, endPoint, currPoint;
	  double distance;
	  public static void resetDist() {
	    QuickStart9eHotlink.milesLabel.setText("DIST   0 mi   ");
	    QuickStart9eHotlink.kmLabel.setText("   0 km    ");
	  }
	  public void mousePressed(MouseEvent me) {
		startx = me.getX(); starty = me.getY();
		dragging = true;
		initPoint = QuickStart9eHotlink.map.transformPixelToWorld(me.getX(),me.getY());
	  }
	  public void mouseReleased(MouseEvent me) {
		endx = me.getX(); endy = me.getY();
		dragging = false;
		endPoint = QuickStart9eHotlink.map.transformPixelToWorld(me.getX(),me.getY());
	    distance = (69.44 / (2*Math.PI)) * 360 * Math.acos(
					 Math.sin(initPoint.y * 2 * Math.PI / 360)
				   * Math.sin(endPoint.y * 2 * Math.PI / 360)
				   + Math.cos(initPoint.y * 2 * Math.PI / 360)
				   * Math.cos(endPoint.y * 2 * Math.PI / 360)
				   * (Math.abs(initPoint.x - endPoint.x) < 180 ?
	                    Math.cos((initPoint.x - endPoint.x)*2*Math.PI/360):
	                    Math.cos((360 - Math.abs(initPoint.x - endPoint.x))*2*Math.PI/360)));
	    System.out.println( distance  );
	    QuickStart9eHotlink.milesLabel.setText("DIST: " + new Float((float)distance).toString() + " mi  ");
	    QuickStart9eHotlink.kmLabel.setText(new Float((float)(distance*1.6093)).toString() + " km");
	    Graphics g = super.getGraphics();
	    g.setColor(Color.blue);
	    g.drawLine(startx,starty,endx,endy);
	  }
	  public void mouseDragged(MouseEvent me) {
		if (dragging) {
	      currx = me.getX();
	      curry = me.getY();
	      Graphics g = super.getGraphics();
	      g.setColor(Color.blue);
	      g.drawLine(startx,starty,currx,curry);
	      super.repaint();
	      try {
			Thread.sleep(50);
	      }
	      catch (Exception e){}
	    }
	  }
	  public void cancel() {};
	}
